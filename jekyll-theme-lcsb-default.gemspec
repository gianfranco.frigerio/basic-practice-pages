# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "jekyll-theme-lcsb-default"
  spec.version       = "0.3.14"
  spec.authors       = ["Trefex", "jaceklebioda_lux"]
  spec.email         = ["lcsb-sysadmins@uni.lu"]

  spec.summary       = "Simple static web-page template for uni.lu"
  spec.homepage      = "https://git-r3lab.uni.lu/core-services/jekyll-theme-lcsb-default"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.0"
  spec.add_runtime_dependency "jekyll-seo-tag", "~> 2.5"

  spec.add_development_dependency "bundler", ">= 2.0"
  spec.add_development_dependency "rake", "~> 12.3"
end
